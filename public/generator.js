"use strict";

// TODO: add MutationObserver
// TODO: add more formatting for library items
// TODO: add more formatting for user submission
// TODO: fix [#], it's not returning the full value

(function() {
    var submitButton = document.getElementById("submit-topic");
    var resultsContainer = document.getElementById("results");
    var libraryData, selectedPhrase = null;

    /**
     * object housing relative information regarding User submitted content
     * @property {boolean} singular
     * @property {string} text
     */
    var topicInput = {};

    var MAX_COUNT = 20;

    // define event lisenters
    // action to perform when User clicks SUBMIT button
    submitButton.addEventListener("click", function() {
        topicInput.text = document.getElementById("topic").value.trim();
        getLibrary();
    }, false);

    // helper functions
    /** 
     * this function randomly choosing a child within the array.
     * @scope Promise
    */
    function choosePhrase() {
        var library = libraryData || storage("get", "library");
        var randomNumber = Math.floor(Math.random() * Math.floor(library.length));

        // set selected phrase
        selectedPhrase = library[randomNumber];
        storage("set", "selection", selectedPhrase);
        displayResults();
    }

    /** 
     * prints selected phrase to User
     * @scope Promise
     */
    function displayResults() {
        var selection = selectedPhrase || storage("get", "selection");
        if (selection) {
            var span = "";
            var index = 0;
            resultsContainer.innerHTML = ""; // clear existing content
            for (var key in selection) {
                if (selection.hasOwnProperty(key)) {
                    if (key == "word_" + (index + 1) && index < 4) {
                        span = document.createElement("span");
                        span.className = "phrase-chunk";
                        span.innerHTML = formatPhrase(selection[key]);
                        resultsContainer.appendChild(span);
                        index++;
                    }
                }
            }
            
        }
    }

    /** 
     * formats the chunk of phrase to form a proper sentence
     * @param {string} phrase
     * @returns {string} formatted phrase
     * @scope Promise
    */
    function formatPhrase(phrase) {
        switch(phrase) {
            case "USER_TERM":
                return formatUserInput();
            case (phrase.includes("is|are") && phrase):
                return topicInput.singular ? "is" : "are";
            case (phrase.includes("isn't|aren't") && phrase):
                return topicInput.singular ? "isn't" : "aren't";
            case (phrase.includes("[#]") && phrase):
                return Math.floor(Math.random() * Math.floor(MAX_COUNT)) + 1;
            default:
                return phrase;
        }
    }

    /** 
     * formats the user input into a proper sentence structure
     * @returns {string} 
     * @scope Promise
    */
    function formatUserInput() {
        // singular/plural check
        topicInput.singular = topicInput.text.endsWith("s") ? false : true;
        
        return topicInput.text;
    }

    /** 
     * fetches JSON object and stores values in local storage
     * @scope eventListener
     */
    function getLibrary() {
        if (!storage("get", "library")) {
            // make an XmlHttpRequest call for a list of objects
            fetch("library.json")
            .then(function(response) {
                return response.json();
            })
            .then(function(data) {
                libraryData = data;
                storage("set", "library", data);
                choosePhrase();
            })
            .catch(function(error) {
                console.error("There was a problem fetching the library file.", error);
            })
        } else {
            choosePhrase();
        }
        
    }

    /** brower's localStorage helper */
    function storage(action, key, value) {
        if (window.localStorage) {
            switch(action) {
                case "set":
                    localStorage.setItem(key, JSON.stringify(value));
                    break;
                default:
                    return JSON.parse(localStorage.getItem(key));
            }
        }
    }
})();
