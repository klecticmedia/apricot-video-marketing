# apricot-video-marketing

The purpose of this project is to replicate the base functionality of [Portent's _title maker_](https://www.portent.com/tools/title-maker).

```bash
+-- public/
|   +-- generator-style.css
|   +-- generator.js
|   +-- index.html
|   +-- library.json
```

## Usage
The files within the `public/` directory is the final product. To run a small web server to test out the functionality run the following commands:

```bash
npm install
npm start
```

## Idea generator logic

* HTML elements on the page are identified.
* An `event listener` is declared on the _submit_ button.

When the submit button is clicked:

1. The `getLibrary` function is called, which checks to see if the data set (library) is already stored in `localStorage`. If not then the flow fetches the JSON file and loads it into memory and saves a copy of it into local storage.
1. The flow then randomly chooses a phrase from the library.
1. The user input is formatted.
1. The chosen phrase is also formatted to fit the user input.
1. Then the results are displayed to the page.

## What's left to do
1. Add more scenarios to format both the user input and certain library items (numbers, singular/plural, etc).
1. Change submit button to a FORM submit button.