var lbgendb;
var lastindex = -1;
var eggs = {"Ian Lurie":"http://www.portent.com/blog/copywriting/10-stupid-blogging-mistakes-me.htm"};
var twits = ["Neat","Nifty","Awesome","Nutty","Killer","Psycho","Sweet"];
$(document).ready(function() {
	$('.blurb').hide();
	$('#diditfail').hide();
	$.get('/wp-content/themes/pi_portent/title-maker/_db/lbgen.xml',
			function(data) {
				lbgendb = $(data);
		    },
		  	'xml');
	$('#submit_btn').click(function() {
		$('#topic').val($.trim($('#topic').val()));
		var topic = $('#topic').val().replace(/[^A-Za-z0-9\s\&\-\']/g, '').replace(/^[\'\&\-]*/, "").replace(/[\'\&\-]*$/,"");
		if (topic == "") return false;
		$(this).addClass("refresh");
		var topic_plural = topic;
		var avoid_singular_headlines = false;
		var use_is_not_are = false;
		if (topic_plural.toLowerCase().indexOf("a ") == 0 || topic_plural.toLowerCase().indexOf("one ") == 0 || topic_plural.toLowerCase().indexOf("the ") == 0) {
			use_is_not_are = true;
		} else if (topic_plural.toLowerCase().indexOf(" and ") < 0 && topic_plural.toLowerCase().indexOf(" or ") < 0) {
			if (topic_plural.indexOf(" ") >= 0) {
				var hoot = owl.pluralize(topic_plural.substring(topic_plural.lastIndexOf(" ") + 1));
				topic_plural = topic_plural.substring(0, topic_plural.lastIndexOf(" ") + 1) + hoot[0];
				use_is_not_are = hoot[1];
			} else {
				var hoot = owl.pluralize(topic_plural);
				topic_plural = hoot[0];
				use_is_not_are = hoot[1];
			}
			avoid_singular_headlines = (topic_plural == topic && ! use_is_not_are);
		}
		var rowindex = lastindex;
		if (url_topic == "DEBUG") {
			rowindex++;
			if (rowindex >= lbgendb.find("Root Row").length)
				rowindex = 0;
		} else {
			while (rowindex == lastindex) {
				rowindex = Math.floor((Math.random() * lbgendb.find("Root Row").length));
				if (rowindex != lastindex && avoid_singular_headlines) {
					var is_singular = (lbgendb.find("Root Row").eq(rowindex).find("singular").text() == 1);
					if (is_singular) {
						rowindex = lastindex;
					}
				}
			}
			if (init_row) {
				rowindex = init_row - 1;
				init_row = false;
			}
		}
		lastindex = rowindex;
		var lbrow = lbgendb.find("Root Row").eq(rowindex);
		var is_singular = (lbrow.find("singular").text() == 1);
		var display_topic = title_case(is_singular ? topic : topic_plural);
		display_topic_2 = (topic in eggs ? "<a href='" + eggs[topic] + "' target='_blank'><img class='egg' style='height:2em;margin-top:-0.6em;' src='img/eggs/" + topic + ".png' /></a>" : display_topic);
		var full_headline = "";
		for (var i = 1; i <= 4; i++) {
			var lb = lbrow.find("blank_"+i).text();
			var is_blank = (lb == "BLANK");
			var tip = $.trim(lbrow.find("tip_"+i).text());
			var dlb = $.trim((is_blank ? display_topic_2 : smart_value(lb, use_is_not_are)));
			$("#blank_"+i).html((url_topic == "DEBUG" && i == 1 ? "[" + (rowindex + 1) + ".] " : "") + dlb);
			$("#tip_"+i).html((! tip && is_blank ? "Your keyword." : tip)).parent().show();
			full_headline += (i != 1 ? " " : "") + (topic in eggs && is_blank ? display_topic : dlb);
		}
		$('#diditfail').show();
		var tweetTxt = twits[Math.floor(Math.random()*twits.length)] + " headline I made with @Portent's #TitleMaker: \"" + full_headline + "\"";
		if (tweetTxt.length > 117)
			tweetTxt = tweetTxt.replace("headline I made", "headline made");
		if (tweetTxt.length > 117)
			tweetTxt = tweetTxt.substring(0, 113) + "...\"";
		var twhref = $('#custom_twitter_link').attr("href");
		twhref = twhref.substring(0, twhref.indexOf("&text=") + 6) + escape(tweetTxt);
		$('#custom_twitter_link').attr("href", twhref);
		$('#custom_twitter_button').css("visibility", "visible");
		if (init_val)
			init_val = false;
		if (location.hostname != "localhost") {
            $.ajax({
				type: "POST",
				url: "/wp-content/themes/pi_portent/title-maker/_db/save.php",
				contentType: "application/x-www-form-urlencoded",
				data: "it="+encodeURIComponent(topic)+"&hid="+rowindex+"&ht="+encodeURIComponent(full_headline),
				error: function(msg) {}, // do nothing, we don't really care if it fails
				complete: function (xhr, stat) {} // do nothing, we don't really care if it succeeds
			});
		} //else alert(full_headline);
	});
	function smart_value(val, use_is_not_are) {
		return val.replace("[#]", (init_val ? init_val : Math.floor(5 + Math.random() * 16))).replace("[is|are]", (use_is_not_are ? "is" : "are")).replace("[s]", (use_is_not_are ? "s" : "")).replace("[s2]", (use_is_not_are ? "" : "s")).replace("[isn't|aren't]", (use_is_not_are ? "isn't" : "aren't")).replace("[it|they]", (use_is_not_are ? "it" : "they"));
	}
	var keep_lower = {"a":1,"an":1,"the":1,"and":1,"but":1,"for":1,"nor":1,"or":1,"so":1,"as":1,"at":1,"by":1,"for":1,"in":1,"of":1,"to":1,"is":1};
	function title_case(txt) {
		var tokens = txt.split(" ");
		var retstr = tokens[0].charAt(0).toUpperCase() + tokens[0].slice(1);
		for (var i = 1; i < tokens.length; i++) {
			var token = tokens[i];
			if (token in keep_lower)
				retstr += " " + token;
			else retstr += " " + token.charAt(0).toUpperCase() + token.slice(1);
		}
		return retstr;
	}
	$('#topic').focus(function() {
		$(this).select();
	});
	$("#topic").keypress(function(event) {
		if (event.which == 13) {
			$('#submit_btn').click();
		} else if (event.which) {
			$('#submit_btn').removeClass("refresh");
		}
	});
	$('form#titleGen').submit(function() {
		return false;
	});
	$.fn.extend({ 
        disableSelection: function() { 
            this.each(function() { 
                if (typeof this.onselectstart != 'undefined') {
                    this.onselectstart = function() { return false; };
                } else if (typeof this.style.MozUserSelect != 'undefined') {
                    this.style.MozUserSelect = 'none';
                } else {
                    this.onmousedown = function() { return false; };
                }
            }); 
        } 
    });
	$('.blurb').disableSelection();
	$('label').disableSelection();
	var urlstr = $.trim(location.search.substring(location.search.indexOf("?") + 1));
	var init_row;
	var init_val;
	if (urlstr != "") {
		var uparams = urlstr.split(".");
		var url_topic = $.trim(uparams[0]);
		if (url_topic != "DEBUG") {
			init_row = (uparams[1] && ! isNaN(parseInt(uparams[1])) ? parseInt(uparams[1]) : false);
			init_val = (uparams[2] && ! isNaN(parseInt(uparams[2])) ? parseInt(uparams[2]) : false);
		}
		if (url_topic != "" && url_topic != "DEBUG") {
			$('#topic').val(decodeURIComponent(url_topic));
			setTimeout(waitForDB, 20);
		}
	}
	function waitForDB() {
		if (lbgendb)
			$('#submit_btn').click();
		else setTimeout(waitForDB, 20);
	}
	$('#custom_twitter_link').click(function() {
		var tw = window.open($(this).attr("href"),"twitterwin","height=250,width=500,directories=no,location=no,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no");
		tw.focus();
		return false;
	});
});